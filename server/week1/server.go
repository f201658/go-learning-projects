package main

import (
	"fmt"
	"net/http"
)

func main() {
	http.HandleFunc("/students", GetAllStudentsHandler)

	http.ListenAndServe(":8080", nil)
}

func GetAllStudentsHandler(w http.ResponseWriter, r *http.Request) {

	if r.Method == "GET" {
		fmt.Println("GET was called")
	} else if r.Method == "POST" {
		fmt.Println("POST was called")
	} else if r.Method == "DELETE" {
		fmt.Println("Delete was called")
	} else {
		fmt.Println("BAD REQUEST NOT SUPPORTED")
	}
}

func GetAllAssignments(w http.ResponseWriter, r *http.Request) {
	displayMessage := "Assignments List:"
	if r.Method == "GET" {
		fmt.Println(displayMessage)
	} else if r.Method == "POST" {
		fmt.Println(displayMessage)
	} else {
		fmt.Println("BAD REQUEST NOT SUPPORTED")
	}
}
