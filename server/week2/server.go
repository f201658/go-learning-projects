package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/students", GetAllStudentsHandler)
	http.HandleFunc("/health", HealthCheck)
	http.HandleFunc("/student", HandleStudent)
	log.Println("Starting server ")
	http.ListenAndServe(":8080", nil)
}

func HandleStudent(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		//get student
		data, err := GetStudent()
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Header().Set("Content-Type", "application/json")
			w.Write([]byte(`{
				"message" : "some error happened"
			}`))
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(data)
		return
	} else if r.Method == "PUT" {
		//update Student'
		readData, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Header().Set("Content-Type", "application/json")
			w.Write([]byte(`{
				"message" : "some error happened"
			}`))
			return
		}

		data, err := UpdateStudent(readData)

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Header().Set("Content-Type", "application/json")
			w.Write([]byte(`{
				"message" : "some error happened"
			}`))
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(data)
		return
	}
}

type Student struct {
	StudentID int    `json:"student_id"`
	Name      string `json:"student_name"`
	Age       int    `json:"student_age"`
	UserName  string `json:"student_username"`
}

func UpdateStudent(data []byte) ([]byte, error) {
	var student Student
	err := json.Unmarshal(data, &student)

	if err != nil {
		return []byte{}, err
	}

	//sudentname_studentid
	student.UserName = fmt.Sprintf("%s_%d", student.Name, student.StudentID)

	jsonData, err := json.Marshal(student)

	return jsonData, err
}

func GetStudent() ([]byte, error) {
	student := Student{StudentID: 1, Name: "Student1", Age: 2}

	jsonData, err := json.Marshal(student)

	return jsonData, err
}

func HealthCheck(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte(`{
		"message" : "ok"
	}`))
}

func GetAllStudentsHandler(w http.ResponseWriter, r *http.Request) {

	if r.Method == "GET" {
		fmt.Println("GET was called")
	} else if r.Method == "POST" {
		fmt.Println("POST was called")
	} else if r.Method == "DELETE" {
		fmt.Println("Delete was called")
	} else {
		fmt.Println("BAD REQUEST NOT SUPPORTED")
	}
}

type ClassRoom struct {
	ClassroomNumber      int    `json:"classroom_number"`
	ClassroomCapacity    int    `json:"classroom_capacity"`
	IsFullClassroom      bool   `json:"isfull_classroom"`
	ClassrooomAssignedTo string `json:"classroom_assignedto"`
}

func GetClassRoom() ([]byte, error) {
	classroom := ClassRoom{ClassroomNumber: 101, ClassroomCapacity: 50, IsFullClassroom: false, ClassrooomAssignedTo: "Smith"}
	jsonData, err := json.Marshal(classroom)

	return jsonData, err
}

func HandleClassRoom(w http.ResponseWriter, r *http.Request) {
	if r.Method == "Get" {
		classroomData, err := GetClassRoom()
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Header().Set("Content-Type", "application/json")
			w.Write([]byte(`{
				"message" : "error getting classroom"
			}`))
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(classroomData)
		return
	} else if r.Method == "PUT" {
		readData, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Header().Set("Content-Type", "application/json")
			w.Write([]byte(`{
				"message" : "some updating classroom"
			}`))
			return
		}

		data, err := UpdateClassroom(readData)

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Header().Set("Content-Type", "application/json")
			w.Write([]byte(`{
				"message" : "some error happened"
			}`))
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(data)
		return
	}
}

func UpdateClassroom(classsRoom []byte) ([]byte, error){
	var classroom ClassRoom
	err := json.Unmarshal(classsRoom, &classroom)
	if err != nil {
		return []byte{}, err
	}
	
	jsonClassroom, err := json.Marshal(classroom)
	return jsonClassroom, err
}
