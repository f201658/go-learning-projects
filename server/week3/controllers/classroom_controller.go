package controllers

import (
	services "go-learning-projects/server/week3/services"
	"io/ioutil"
	"net/http"
)

func HandleClassRoom(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		classroomData, err := services.GetClassRoom()
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Header().Set("Content-Type", "application/json")
			w.Write([]byte(`{
				"message" : "error getting classroom"
			}`))
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(classroomData)
		return
	} else if r.Method == "PUT" {
		readData, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Header().Set("Content-Type", "application/json")
			w.Write([]byte(`{
				"message" : "some updating classroom"
			}`))
			return
		}

		data, err := services.UpdateClassroom(readData)

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Header().Set("Content-Type", "application/json")
			w.Write([]byte(`{
				"message" : "some error happened"
			}`))
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(data)
		return
	}
}