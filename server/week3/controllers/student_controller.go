package controllers

import (
	services "go-learning-projects/server/week3/services"
	"io/ioutil"
	"net/http"
)

func HandleStudent(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		//get student
		data, err := services.GetStudent()
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Header().Set("Content-Type", "application/json")
			w.Write([]byte(`{
				"message" : "some error happened"
			}`))
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(data)
		return
	} else if r.Method == "PUT" {
		//update Student'
		readData, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Header().Set("Content-Type", "application/json")
			w.Write([]byte(`{
				"message" : "some error happened"
			}`))
			return
		}

		data, err := services.UpdateStudent(readData)

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Header().Set("Content-Type", "application/json")
			w.Write([]byte(`{
				"message" : "some error happened"
			}`))
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(data)
		return
	}
}
