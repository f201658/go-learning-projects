package models

type ClassRoom struct {
	ClassroomNumber      int    `json:"classroom_number"`
	ClassroomCapacity    int    `json:"classroom_capacity"`
	IsFullClassroom      bool   `json:"isfull_classroom"`
	ClassrooomAssignedTo string `json:"classroom_assignedto"`
}