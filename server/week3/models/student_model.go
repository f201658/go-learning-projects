package models

type Student struct {
	StudentID int    `json:"student_id"`
	Name      string `json:"student_name"`
	Age       int    `json:"student_age"`
	UserName  string `json:"student_username"`
}
