package main

import (
	"go-learning-projects/server/week3/controllers"
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/health", controllers.HealthCheck)
	http.HandleFunc("/student", controllers.HandleStudent)
	log.Println("Starting server ")
	http.ListenAndServe(":8080", nil)
}
