package services

func GetClassRoom() ([]byte, error) {
	classroom := models.ClassRoom{ClassroomNumber: 101, ClassroomCapacity: 50, IsFullClassroom: false, ClassrooomAssignedTo: "Smith"}
	jsonData, err := json.Marshal(classroom)

	return jsonData, err
}

func UpdateClassroom(classsRoom []byte) ([]byte, error){
	var classroom models.ClassRoom
	err := json.Unmarshal(classsRoom, &classroom)
	if err != nil {
		return []byte{}, err
	}
	
	jsonClassroom, err := json.Marshal(classroom)
	return jsonClassroom, err
}