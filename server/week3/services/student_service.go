package services

import (
	"encoding/json"
	"fmt"
	models "go-learning-projects/server/week3/models"
)

func UpdateStudent(data []byte) ([]byte, error) {
	var student models.Student
	err := json.Unmarshal(data, &student)

	if err != nil {
		return []byte{}, err
	}

	//sudentname_studentid
	student.UserName = fmt.Sprintf("%s_%d", student.Name, student.StudentID)

	jsonData, err := json.Marshal(student)

	return jsonData, err
}

func GetStudent() ([]byte, error) {
	student := models.Student{StudentID: 1, Name: "Student1", Age: 2}

	jsonData, err := json.Marshal(student)

	return jsonData, err
}
