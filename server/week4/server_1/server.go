package main

import (
	"log"
	"net/http"
)

func main() {

	mux := http.NewServeMux()

	mux.HandleFunc("/hello", helloHandler)
	mux.HandleFunc("/bye", byeHandler)

	// Create a server listening on port 8000
	s := &http.Server{
		Addr:    ":8000",
		Handler: mux,
	}

	// Continue to process new requests until an error occurs
	log.Fatal(s.ListenAndServe())
}

func helloHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, "Method not supported", http.StatusMethodNotAllowed)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte(`{
				"message" : "Hello !"
			}`))

}

func byeHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "DELETE" {
		http.Error(w, "Method not supported", http.StatusMethodNotAllowed)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte(`{
				"message" : "Bye !"
			}`))

}
