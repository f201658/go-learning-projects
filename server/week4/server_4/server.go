package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()

	router.GET("/hello", helloHandler)
	router.POST("/hello", postHandler)
	router.PUT("/hello", putHandler)
	router.DELETE("/bye", byeHandler)

	router.Run(":8080")
}

func helloHandler(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"message": "hello",
	})
}

func byeHandler(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"message": "bye !",
	})
}

func postHandler(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"message": "post !",
	})
}
func putHandler(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"message": "put !",
	})
}
